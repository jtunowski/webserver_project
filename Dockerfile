FROM python:3.8-slim

WORKDIR /webserver

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./app /webserver/app

CMD ["uvicorn", "app.main:webserver", "--host", "0.0.0.0", "--port", "8000"]