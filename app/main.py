from fastapi import FastAPI, status, Response, HTTPException, Query

webserver = FastAPI()


@webserver.get("/-/health", status_code=status.HTTP_200_OK)
def healthcheck():
    return 1


@webserver.get("/api/echo")
def echo(response: Response, text):
    if text:
        response.status_code = status.HTTP_200_OK
        return {"text": text}
    else:
        raise HTTPException(status_code=404, detail="Wrong API parameter")
