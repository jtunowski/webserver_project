from fastapi.testclient import TestClient
from app.main import webserver

test_connection = TestClient(webserver)


def test_healthcheck():
    response = test_connection.get("/-/health")
    assert response.status_code == 200
    assert response.text == "1"


def test_happy_path_for_echo_api():
    response = test_connection.get("/api/echo?text=TestClient")
    assert response.status_code == 200
    assert response.json() == {"text": "TestClient"}


def test_for_echo_api_wrong_parameter():
    response = test_connection.get("/api/echo?text=")
    assert response.status_code == 404
    assert response.json() == {"detail": "Wrong API parameter"}


def test_for_echo_api_default_return():
    response = test_connection.get("/api/echo")
    assert response.status_code == 422
